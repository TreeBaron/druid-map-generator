﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Druid
{
    class Textures
    {
        private static List<Texture2D> textures = new List<Texture2D>();
        private static List<string> texturenames = new List<string>();

        public static void AddTexture(string name, Texture2D texture)
        {
            if(name == "" || name == null)
            {
                throw new Exception("Texture name not valid!");
            }
            name = name.ToLower();
            texturenames.Add(name);
            textures.Add(texture);
        }

        public static Texture2D GetTexture(string name)
        {
            name = name.ToLower();
            for(int i = 0; i < texturenames.Count; i++)
            {
                if(texturenames[i] == name)
                {
                    return textures[i];
                }
            }

            throw new Exception("Texture could not be found!");
        }

    }
}
