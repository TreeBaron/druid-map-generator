﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Druid
{
    class GameWorld
    {
        List<List<Tile>> WorldMap = new List<List<Tile>>();

        List<List<Tile>> ActiveMap = new List<List<Tile>>();

        private List<Entity> WorldEntities = new List<Entity>();

        private enum RiverSlant
        {
            SlantUpLeft,
            SlantUpRight,
            SlantDownRight,
            SlantDownLeft
        }

        //default block height and width
        private int BlockWidth = 0;
        private int BlockHeight = 0;

        public void DrawWorld(SpriteBatch SB, Vector2 CameraPosition)
        {
            for (int x = 0; x < ActiveMap.Count; x++)
            {
                for (int y = 0; y < ActiveMap[x].Count; y++)
                {
                    //check if we are in draw distance :P
                    if (Vector2.Distance(ActiveMap[x][y].Position, CameraPosition) < Settings.DrawDistance)
                    {
                        ActiveMap[x][y].EntityDraw(SB);
                    }
                }
            }

            //syntax!
            for (int x = 0; x < ActiveMap.Count; x++)
            {
                for (int y = 0; y < ActiveMap[x].Count; y++)
                {
                    //check if we are in draw distance :P
                    if (Vector2.Distance(ActiveMap[x][y].Position, CameraPosition) < Settings.DrawDistance)
                    {
                        ActiveMap[x][y].SubEntityDraw(SB);
                    }
                }
            }
        }

        public void UpdateWorld(GameTime GT, Vector2 CameraPosition)
        {
            //very important...
            UpdateActiveMap(CameraPosition);
            for (int x = 0; x < ActiveMap.Count; x++)
            {
                for (int y = 0; y < ActiveMap[x].Count; y++)
                {
                    ActiveMap[x][y].EntityUpdate(GT);
                }
            }
        }

        public void UpdateActiveMap(Vector2 Position)
        {
            //block ~position player is at
            int x = (int)Position.X / BlockWidth;
            int y = (int)Position.Y / BlockHeight;

            //check how many blocks go into screen width...
            int BlockScreenWidth = Settings.ScreenWidth / (BlockWidth * 3);
            int BlockScreenHeight = Settings.ScreenWidth / (BlockHeight * 3);
            int LeftPull = BlockScreenHeight + BlockScreenWidth;

            x -= LeftPull;
            y -= LeftPull;

            //clear active map
            ActiveMap.Clear();
            ActiveMap.Add(new List<Tile>());

            x = Math.Max(0, x);
            y = Math.Max(0, y);

            for (int i = 0; i < Math.Min(LeftPull * 2, WorldMap.Count); i++)
            {
                ActiveMap.Add(new List<Tile>());
                for (int j = 0; j < Math.Min(LeftPull * 2, WorldMap[0].Count); j++)
                {
                    int selectx = Math.Min(x + i, WorldMap.Count - 1);
                    int selecty = Math.Min(y + j, WorldMap[0].Count - 1);
                    ActiveMap[i].Add(WorldMap[selectx][selecty]);
                }
                //reset y var
                //multiply by 2 here to offset ratio of block height to width
                y = (int)(Position.Y / BlockHeight) * 2;
                y -= LeftPull;
                y = Math.Max(0, y);
            }

            //complete
        }

        public void FillWorld(Tile E, int worldwidth, int worldheight)
        {
            E.UpdateDrawBox();
            BlockHeight = E.DrawBox.Height;
            BlockWidth = E.DrawBox.Width;
            int TotalCount = 0;
            //set entities into world map
            for (int x = 0; x < worldwidth; x++)
            {
                WorldMap.Add(new List<Tile>());
                for (int y = 0; y < worldheight; y++)
                {
                    Tile EC = Tile.GetShallowCopy(E);
                    EC.X = x;
                    EC.Y = y;

                    WorldMap[x].Add(EC);
                    TotalCount++;
                }
            }

            float Modx = 0;
            //set entities actual position based on world map setup
            for (int x = 0; x < WorldMap.Count; x++)
            {
                if (x >= 1)
                {
                    Modx += E.DrawBox.Width;
                }

                float ModY = 0;
                for (int y = 0; y < WorldMap[x].Count; y++)
                {
                    if (y >= 1)
                    {
                        ModY += E.DrawBox.Height / 2;
                    }

                    WorldMap[x][y].Position = new Vector2(Modx, ModY);

                    if (y % 2 == 0)
                    {
                        Vector2 pos = WorldMap[x][y].Position;
                        WorldMap[x][y].Position = new Vector2(pos.X + (E.DrawBox.Width / 2), pos.Y);
                    }

                    //set worldmap reference position
                    WorldMap[x][y].WorldMapX = x;
                    WorldMap[x][y].WorldMapY = y;
                }
            }

            Console.WriteLine("Generated World of Size: " + String.Format("{0:n0}", TotalCount));
            Console.WriteLine("X = " + WorldMap.Count);
            Console.WriteLine("Y = " + WorldMap[0].Count);
        }

        public void AddHouses(Tile house, Resource resourceNeeded, int resourceAmountThreshold = 1, int amount = 150)
        {
            for (int i = 0; i < amount; i++)
            {
                Tile tile = GetRandomTile();
                //if we have people nearby...
                if (GetTotalResourceAvailability(GetResources(100, tile.WorldMapX, tile.WorldMapY), resourceNeeded) > resourceAmountThreshold)
                {
                    this.AddSubStructure(house, tile.WorldMapX, tile.WorldMapY);
                }
            }
        }

        public void AddResourceSplotch(Resource people, int radius)
        {
            GetRandomTile();
            List<Tile> tiles = GetTilesInRange(radius, GetRandomTile().Position);

            //add resource to tiles...
            foreach (Tile tile in tiles)
            {
                tile.ResourceAmount = new ResourceCounter(people, 5);
            }
        }

        private Tile GetRandomTile()
        {
            return WorldMap[Settings.R.Next(0, GetWorldTileWidth())][Settings.R.Next(0, GetWorldTileHeight())];
        }

        /// <summary>
        /// Adds dry grass to level based on water availability...
        /// Converts from normal grass to dry grass, does not change beyond that
        /// </summary>
        /// <param name="grass"></param>
        /// <param name="drygrass"></param>
        public void AddDryGrass(Tile grass, Tile drygrass)
        {
            Console.WriteLine("Beginning Grass Placement Calculations...");
            const int radi = 12;
            int waterCount = 0;
            //set entities into world map
            for (int x = 0; x < GetWorldTileWidth(); x += radi)
            {
                for (int y = 0; y < GetWorldTileHeight(); y += radi)
                {
                    //update water count...
                    if (y % radi == 0 || x % radi == 0 || x == GetWorldTileWidth() - 1 || y == GetWorldTileHeight() - 1)
                    {
                        waterCount = ResourceCountInRange(MasterResourceDictionary.GetResource("water"), 50, x, y);
                    }

                    //if the current block is type grass
                    if (WorldMap[x][y].Name == grass.Name)
                    {
                        //if there's not enough water in range
                        if (waterCount < 1)
                        {
                            //replace grass with dry grass
                            BrushStar(drygrass, (radi + 1) * BlockWidth, WorldMap[x][y].Position);
                        }
                    }
                }
                double percent = (100.0 / GetWorldTileWidth()) * x;
                Console.WriteLine("%" + Math.Round(percent, 2));
            }
        }

        public void AddSubEntitiesScattered(Tile tileBase, Tile subEntity, int outOfRandom = 101)
        {
            Console.WriteLine("Beginning Grass Placement Calculations...");
            //set entities into world map
            for (int x = 0; x < GetWorldTileWidth(); x++)
            {
                for (int y = 0; y < GetWorldTileHeight(); y++)
                {
                    //if the current block is type grass
                    if (WorldMap[x][y].Name == tileBase.Name && Settings.R.Next(0, outOfRandom) == 1)
                    {
                        Entity placer = Entity.GetShallowCopy(subEntity);
                        WorldMap[x][y].SubEntities.Add(placer);
                        placer.Position = WorldMap[x][y].Position;
                    }
                }
                double percent = (100.0 / GetWorldTileWidth()) * x;
                Console.WriteLine("%" + Math.Round(percent, 2));
            }
        }

        /// <summary>
        /// places a structure on the map, starting at upper left corner...
        /// </summary>
        /// <param name="structure"></param>
        public void AddStructure(List<List<Tile>> structure, int xStart, int yStart)
        {
            //a list of rows...
            for (int y = 0; y < structure.Count; y++)
            {
                for (int x = 0; x < structure[0].Count; x++)
                {
                    if (y < structure.Count && x < structure[0].Count)
                    {
                        //if a place exists to place the tile
                        if (x + xStart >= 0 && y + yStart >= 0 && x + xStart < GetWorldTileWidth() && y + yStart < GetWorldTileHeight())
                        {
                            //place tile
                            Vector2 BlockPosition = WorldMap[xStart + x][yStart + y].Position;
                            Tile instance = Tile.GetShallowCopy(structure[y][x]);
                            //draw our tile here...
                            WorldMap[x + xStart][y + yStart] = instance;
                            instance.Position = BlockPosition;
                            instance.WorldMapX = x + xStart;
                            instance.WorldMapY = y + yStart;
                            instance.UpdateDrawBox();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// places a structure on the map, starting at upper left corner...
        /// </summary>
        /// <param name="structure"></param>
        public void AddSubStructure(Tile structure, int xStart, int yStart)
        {
            //place tile
            Vector2 BlockPosition = WorldMap[xStart][yStart].Position;
            Tile instance = Tile.GetShallowCopy(structure);
            //draw our tile here...
            WorldMap[xStart][yStart].SubEntities.Add(instance);
            instance.Position = BlockPosition;
            instance.WorldMapX = xStart;
            instance.WorldMapY = yStart;
            instance.UpdateDrawBox();

            //get texture size, and remove tile sub-entities around it
            //practically this means clearing bushes and trees too close, etc
            int width = structure.Texture.Width;
            int height = structure.Texture.Height;
            int tileClearWidth = width / BlockWidth;
            int tileClearHeight = height / BlockHeight;

            for (int x = 0; x < tileClearWidth; x++)
            {
                for (int y = 0; y < tileClearHeight; y++)
                {
                    try
                    {
                        if (x != 0 || y != 0)
                        {
                            WorldMap[xStart + x][yStart + y].SubEntities.Clear();
                        }
                    }
                    catch (Exception e)
                    {
                        //ignore
                    }
                }
            }
        }

        /// <summary>
        /// Adds dry grass to level based on water availability...
        /// Converts from normal grass to dry grass, does not change beyond that
        /// </summary>
        /// <param name="drygrass"></param>
        /// <param name="sand"></param>
        public void AddSands(Tile drygrass, Tile sand)
        {
            Console.WriteLine("Beginning Sand Placement Calculations...");
            const int radi = 12;
            int waterCount = 0;
            //set entities into world map
            for (int x = 0; x < GetWorldTileWidth(); x += radi)
            {
                for (int y = 0; y < GetWorldTileHeight(); y += radi)
                {
                    //update water count...
                    if (y % radi == 0 || x % radi == 0 || x == GetWorldTileWidth() - 1 || y == GetWorldTileHeight() - 1)
                    {
                        waterCount = ResourceCountInRange(MasterResourceDictionary.GetResource("water"), 75, x, y);
                    }

                    //if the current block is type grass
                    if (WorldMap[x][y].Name == drygrass.Name)
                    {

                        //if there's not enough water in range
                        if (waterCount < 1)
                        {
                            //replace grass with dry grass
                            BrushStar(sand, (radi + 1) * BlockWidth, WorldMap[x][y].Position);
                        }
                    }
                }
                double percent = (100.0 / GetWorldTileWidth()) * x;
                Console.WriteLine("%" + Math.Round(percent, 2));
            }
        }

        /// <summary>
        /// Adds dry grass to level based on water availability...
        /// Converts from normal grass to dry grass, does not change beyond that
        /// </summary>
        /// <param name="grass"></param>
        /// <param name="drygrass"></param>
        public void AddTrees(Tile grass, Tile tree)
        {
            Console.WriteLine("Beginning Tree Placement Calculations...");
            const int radi = 10;
            int waterCount = 0;
            //set entities into world map
            for (int x = 0; x < GetWorldTileWidth(); x += radi)
            {
                for (int y = 0; y < GetWorldTileHeight(); y += radi)
                {
                    //update water count...
                    if (y % radi == 0 || x % radi == 0 || x == GetWorldTileWidth() - 1 || y == GetWorldTileHeight() - 1)
                    {
                        waterCount = ResourceCountInRange(MasterResourceDictionary.GetResource("water"), 100, x, y);
                    }

                    //if the current block is type grass
                    if (WorldMap[x][y].Name == grass.Name)
                    {
                        //if there's not enough water in range
                        if (waterCount >= 5)
                        {
                            //replace grass with dry grass
                            BrushTrees(tree, grass, (radi + 1) * BlockWidth, WorldMap[x][y].Position);
                        }
                    }
                }
                double percent = (100.0 / GetWorldTileWidth()) * x;
                Console.WriteLine("%" + Math.Round(percent, 2));
            }
        }


        /// <summary>
        /// Good for lakes and "oceans"
        /// </summary>
        /// <param name="water"></param>
        /// <param name="sand"></param>
        public void AddLake(Tile water, Tile sand, int blockRadius, int count)
        {
            for (int i = 0; i < count; i++)
            {
                this.BrushCircle(water, (float)(blockRadius * BlockWidth), this.RandomWorldTilePosition());
            }
        }

        public void AddRiver(Tile water, Tile sand)
        {
            int y = Settings.R.Next(0, GetWorldTileHeight());
            int x = 0;
            AddRiverActual(water, sand, x, y);
        }

        private void AddRiverActual(Tile water, Tile sand, int x, int y, int variance = 8, int radius = 16, bool canSplit = true)
        {
            //abort if too small
            if (radius <= 1)
            {
                return;
            }
            //left to right...
            for (x = 0; x < GetWorldTileWidth(); x += 6)
            {
                y += Settings.R.Next(-variance, variance);
                y = Math.Abs(y);
                y = Math.Min(GetWorldTileHeight() - 1, y);
                this.BrushCircle(water, radius * BlockWidth, WorldMap[x][y].Position);
            }
        }

        public void GenerateBeaches(Tile sand, Tile water)
        {
            for (int x = 0; x < GetWorldTileWidth(); x++)
            {
                for (int y = 0; y < GetWorldTileHeight(); y++)
                {
                    //if we are not water, and not sand...
                    if (WorldMap[x][y].Name != sand.Name && WorldMap[x][y].Name != water.Name)
                    {
                        //make sure boundaries are okay for check
                        if (x - 1 > 0 && x + 1 < GetWorldTileWidth() && y - 1 > 0 && y + 1 < GetWorldTileHeight())
                        {
                            //if one of the tiles around us is water, make ourselves sand
                            Tile up = WorldMap[x][y + 1];
                            Tile down = WorldMap[x][y - 1];
                            Tile left = WorldMap[x - 1][y];
                            Tile right = WorldMap[x + 1][y];

                            if (up.Name == water.Name || down.Name == water.Name || left.Name == water.Name || right.Name == water.Name)
                            {
                                //make ourselves...sand
                                Vector2 BlockPosition = WorldMap[x][y].Position;
                                Tile instance = Tile.GetShallowCopy(sand);
                                //draw our tile here...
                                WorldMap[x][y] = instance;
                                instance.Position = BlockPosition;
                                instance.UpdateDrawBox();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// A perfect circle brush.
        /// </summary>
        /// <param name="entityTile"></param>
        /// <param name="radius"></param>
        /// <param name="origin"></param>
        public void BrushCircle(Tile entityTile, float radius, Vector2 origin)
        {
            List<Tile> inRange = GetTilesInRange(radius, origin);
            for (int i = 0; i < inRange.Count; i++)
            {
                Vector2 BlockPosition = inRange[i].Position;
                Tile instance = Tile.GetShallowCopy(entityTile);
                //draw our tile here...
                WorldMap[inRange[i].WorldMapX][inRange[i].WorldMapY] = instance;
                instance.Position = BlockPosition;
                instance.WorldMapX = inRange[i].WorldMapX;
                instance.WorldMapY = inRange[i].WorldMapY;
                instance.UpdateDrawBox();
            }
        }

        /// <summary>
        /// A perfect circle brush.
        /// </summary>
        /// <param name="entityTile"></param>
        /// <param name="radius"></param>
        /// <param name="origin"></param>
        public void BrushScatteredCircle(Tile entityTile, float radius, Vector2 origin)
        {
            List<Tile> inRange = GetTilesInRange(radius, origin);
            for (int i = 0; i < inRange.Count; i++)
            {
                if (Settings.R.NextDouble() < 0.66)
                {
                    Vector2 BlockPosition = inRange[i].Position;
                    Tile instance = Tile.GetShallowCopy(entityTile);
                    //draw our tile here...
                    WorldMap[inRange[i].WorldMapX][inRange[i].WorldMapY] = instance;
                    instance.Position = BlockPosition;
                    instance.WorldMapX = inRange[i].WorldMapX;
                    instance.WorldMapY = inRange[i].WorldMapY;
                    instance.UpdateDrawBox();
                }
            }
        }

        /// <summary>
        /// A perfect circle brush.
        /// </summary>
        /// <param name="entityTile"></param>
        /// <param name="radius"></param>
        /// <param name="origin"></param>
        public void BrushTrees(Tile entityTile, Tile placeType, float radius, Vector2 origin)
        {
            List<Tile> inRange = GetTilesInRange(radius, origin);
            for (int i = 0; i < inRange.Count; i++)
            {
                if (Settings.R.Next(0, 16) == 5 && inRange[i].Name == placeType.Name && WorldMap[inRange[i].WorldMapX][inRange[i].WorldMapY].SubEntities.Count < 1)
                {
                    Vector2 BlockPosition = inRange[i].Position;
                    Tile instance = Tile.GetShallowCopy(entityTile);
                    //draw our tile here...
                    WorldMap[inRange[i].WorldMapX][inRange[i].WorldMapY].SubEntities.Add(instance);
                    instance.Position = BlockPosition;
                    instance.WorldMapX = inRange[i].WorldMapX;
                    instance.WorldMapY = inRange[i].WorldMapY;
                    instance.UpdateDrawBox();
                }
            }
        }

        /// <summary>
        /// A perfect circle brush.
        /// </summary>
        /// <param name="entityTile"></param>
        /// <param name="radius"></param>
        /// <param name="origin"></param>
        public void BrushSquare(Tile entityTile, float radius, Vector2 origin)
        {
            List<Tile> inRange = GetTilesInRangeSquare(radius, origin);
            for (int i = 0; i < inRange.Count; i++)
            {
                Vector2 BlockPosition = inRange[i].Position;
                Tile instance = Tile.GetShallowCopy(entityTile);
                //draw our tile here...
                WorldMap[inRange[i].WorldMapX][inRange[i].WorldMapY] = instance;
                instance.Position = BlockPosition;
                instance.WorldMapX = inRange[i].WorldMapX;
                instance.WorldMapY = inRange[i].WorldMapY;
                instance.UpdateDrawBox();
            }
        }

        /// <summary>
        /// Designed for ocean and lake drawing. Smoother than BrushStarFracture
        /// </summary>
        /// <param name="entityTile"></param>
        /// <param name="radius"></param>
        /// <param name="origin"></param>
        public void BrushStar(Tile entityTile, float radius, Vector2 origin)
        {
            radius *= 2;
            int scatter = 5;
            for (int i = 0; i < scatter; i++)
            {
                Vector2 newPosition = origin + new Vector2(Settings.R.Next(-1 * (int)(radius / scatter), (int)(radius / scatter)), Settings.R.Next(-1 * (int)(radius / scatter), (int)(radius / scatter)));

                if (Settings.R.Next(0, 6) == 3)
                {
                    BrushScatteredCircle(entityTile, radius * 0.35f, newPosition);
                }
                else if (Settings.R.Next(0, 11) == 3)
                {
                    BrushSquare(entityTile, radius * 0.35f, newPosition);
                }
                else
                {
                    BrushCircle(entityTile, radius * 0.35f, newPosition);
                }

            }
        }

        public Vector2 RandomWorldTilePosition()
        {
            return WorldMap[Settings.R.Next(0, GetWorldTileWidth())][Settings.R.Next(0, GetWorldTileHeight())].Position;
        }

        /// <summary>
        /// Returns the center of the map relative to block count, not XNA coordinate scheme.
        /// </summary>
        /// <returns></returns>
        public Vector2 GetCenter()
        {
            return new Vector2(WorldMap.Count / 2, WorldMap[0].Count / 2);
        }

        /// <summary>
        /// Gets all available resources from blocks in a certain block radius.
        /// </summary>
        /// <param name="blockRadius"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public List<ResourceCounter> GetResources(int blockRadius, int blockX, int blockY)
        {
            List<ResourceCounter> resources = new List<ResourceCounter>();
            for (int x = (int)(blockX - blockRadius); x < blockX + blockRadius; x++)
            {
                for (int y = (int)(blockY - blockRadius); y < blockY + blockRadius; y++)
                {
                    //only attempt within bounds of world map
                    if (x >= 0 && y >= 0 && x < GetWorldTileWidth() && y < GetWorldTileHeight())
                    {
                        //if the resource amount is not null...
                        if (WorldMap[x][y].ResourceAmount != null)
                        {
                            resources.Add(WorldMap[x][y].ResourceAmount);
                        }
                    }
                }
            }
            return resources;
        }

        //given a resource, a block range, and a position returns how many resources are in that range
        public int ResourceCountInRange(Resource resource, int blockRange, int blockX, int blockY)
        {
            List<ResourceCounter> resources = GetResources(blockRange, blockX, blockY);
            return GetTotalResourceAvailability(resources, resource);
        }

        public int GetTotalResourceAvailability(List<ResourceCounter> resources, Resource resource)
        {
            int count = 0;
            foreach (ResourceCounter counter in resources)
            {
                if (counter.Name == resource.Name)
                {
                    count++;
                }
            }
            return count;
        }

        public int GetWorldTileWidth()
        {
            return WorldMap.Count;
        }

        public int GetWorldTileHeight()
        {
            return WorldMap[0].Count;
        }

        public double GetWorldXNAWidth()
        {
            return BlockWidth * WorldMap.Count;
        }

        public double GetWorldXNAHeight()
        {
            return BlockHeight * WorldMap[0].Count;
        }

        public bool PlaceObject(int x, int y, Entity E)
        {
            x = Math.Max(0, x);
            y = Math.Max(0, y);
            x = Math.Min(x, WorldMap.Count - 1);
            y = Math.Min(y, WorldMap[0].Count - 1);

            if (!WorldEntities.Contains(E))
            {
                WorldEntities.Add(E);
            }

            E.GameWorld = this;

            if (IsEmpty(x, y) == false)
            {
                return false;
            }

            WorldMap[x][y].SubEntities.Add(E);
            E.Position = WorldMap[x][y].Position;
            E.X = x;
            E.Y = y;

            return true;

        }

        public bool PlaceObject(Vector2 V, Entity E)
        {
            int x = (int)V.X / BlockWidth;
            int y = (int)V.Y / BlockHeight;

            if ((double)V.X % BlockWidth > 0)
            {
                x++;
            }

            if ((double)V.Y % BlockHeight > 0)
            {
                y++;
            }

            if (IsEmpty(x, y) == false)
            {
                return false;
            }

            return PlaceObject(x, y, E);
        }

        public bool IsEmpty(int x, int y)
        {
            if (WorldMap[x][y].SubEntities.Count <= 0)
            {
                return true;
            }
            return false;
        }

        public List<Entity> GetSubEntities(int x, int y)
        {
            x = Math.Max(0, x);
            y = Math.Max(0, y);
            x = Math.Min(x, WorldMap.Count);
            y = Math.Min(y, WorldMap[0].Count);

            return WorldMap[x][y].SubEntities;
        }

        public Entity GetEntityAt(int x, int y)
        {
            x = Math.Max(0, x);
            y = Math.Max(0, y);
            x = Math.Min(x, WorldMap.Count);
            y = Math.Min(y, WorldMap[0].Count);

            return WorldMap[x][y];
        }

        public void EndTurn(int amount = 1)
        {
            AddToEntityDebt(amount);
        }

        public void AddToEntityDebt(int amount)
        {
            foreach (Entity E in WorldEntities)
            {
                if (E != null)
                {
                    E.StepDebt += amount;
                }
            }
        }

        public List<Tile> GetTilesInRange(float radius, Vector2 origin)
        {
            List<Tile> inRange = new List<Tile>();
            for (int x = 0; x < GetWorldTileWidth(); x++)
            {
                for (int y = 0; y < GetWorldTileHeight(); y++)
                {
                    if (Vector2.Distance(origin, WorldMap[x][y].Position) <= radius)
                    {
                        inRange.Add(WorldMap[x][y]);
                    }
                }
            }
            return inRange;
        }

        public List<Tile> GetTilesInRangeSquare(float radius, Vector2 origin)
        {
            List<Tile> inRange = new List<Tile>();
            for (int x = 0; x < GetWorldTileWidth(); x++)
            {
                for (int y = 0; y < GetWorldTileHeight(); y++)
                {
                    if (Math.Abs(origin.X - WorldMap[x][y].Position.X) <= radius && Math.Abs(origin.Y - WorldMap[x][y].Position.Y) <= radius)
                    {
                        inRange.Add(WorldMap[x][y]);
                    }
                }
            }
            return inRange;
        }
    }
}
