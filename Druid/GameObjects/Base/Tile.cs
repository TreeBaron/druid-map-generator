﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Druid
{
    class Tile : Entity
    {
        /// <summary>
        /// Tracks the amount of this resource in this tile.
        /// </summary>
        public ResourceCounter ResourceAmount
        {
            get;
            set;
        }

        public int WorldMapX
        {
            get;
            set;
        }

        public int WorldMapY
        {
            get;
            set;
        }

        public Tile(string name, Texture2D texture, int x, int y) : base(name, texture, x, y)
        {

        }

        public override void Draw(SpriteBatch SB)
        {
            base.Draw(SB);
        }

        public override void Update(GameTime GT)
        {
            base.Update(GT);
        }

        public static Tile GetShallowCopy(Tile E)
        {
            Tile Clone = (Tile)E.MemberwiseClone();
            Clone.SubEntities = new List<Entity>();

            foreach (Entity e in E.SubEntities)
            {
                Clone.SubEntities.Add(Entity.GetShallowCopy(e));
            }

            Clone.ResourceAmount = E.ResourceAmount;

            return Clone;
        }

    }
}
