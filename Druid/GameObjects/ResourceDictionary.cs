﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Druid
{
    public static class MasterResourceDictionary
    {
        public static List<Resource> AllResourceTypes = new List<Resource>();

        public static Resource GetResource(string name)
        {
            name = name.ToLower();
            foreach (Resource r in AllResourceTypes)
            {
                if (r.Name == name)
                {
                    return r;
                }
            }
            throw new Exception("Could not find resource in dictionary!");
        }

        public static bool ResourceExists(string name)
        {
            name = name.ToLower();
            foreach (Resource r in AllResourceTypes)
            {
                if (r.Name == name)
                {
                    return true;
                }
            }
            return false;
        }

    }

    public class Resource
    {
        public string Name
        {
            get;
            set;
        }

        public Resource(string name)
        {
            Name = name.ToLower();

            if (MasterResourceDictionary.ResourceExists(name) == false)
            {
                //add ourselves to the master dictionary of resources
                MasterResourceDictionary.AllResourceTypes.Add(this);
            }
            else
            {
                throw new Exception("A resource of that name already exists.");
            }

        }

    }

    public class ResourceCounter
    {
        public ResourceCounter(Resource resource, int amount)
        {
            Name = resource.Name;
            Amount = amount;
        }

        private string m_name;

        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value.ToLower();
            }
        }

        public int Amount
        {
            get;
            set;
        }

        public Resource ResourceActual
        {
            get
            {
                return MasterResourceDictionary.GetResource(this.Name);
            }
        }

        //duplicate this resource
        //this is for all purposes involved a deep copy
        public ResourceCounter GetCopy()
        {
            return new ResourceCounter(ResourceActual, Amount);
        }
    }
}
