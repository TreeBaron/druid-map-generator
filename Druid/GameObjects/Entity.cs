﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Druid
{
    abstract class Entity
    {
        public bool UpdateEntity { get; set; }
        public bool DrawEntity { get; set; }
        public Texture2D Texture { get; set; }
        public string Name { get; set; }
        public Vector2 Position { get; set; }
        public float Scale { get; set; }
        public Rectangle DrawBox { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int MaxHealth { get; set; }
        public int Health { get; set; }
        public string ClassName { get; set; }
        public GameWorld GameWorld { get; set; }

        public int StepDebt = 1;


        public Entity Owner { get; set; }
        public bool MoveWithOwner { get; set; }

        public List<Entity> SubEntities = new List<Entity>();

        public Entity(string name, Texture2D texture, int x, int y)
        {
            X = x;
            Y = y;
            Name = name;
            Texture = texture;
            UpdateEntity = true;
            DrawEntity = true;
            Position = new Vector2(0, 0);
            Scale = 1.0f;
            if (texture != null)
            {
                DrawBox = new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
            }
            MaxHealth = 100;
            Health = 100;
            ClassName = "entity";
        }

        public Entity(string name, int x = 0, int y = 0)
        {
            X = x;
            Y = y;
            Name = name;
            UpdateEntity = true;
            DrawEntity = true;
            Position = new Vector2(0, 0);
            Scale = 1.0f;
            MaxHealth = int.MaxValue;
            Health = MaxHealth;
            ClassName = "entity";
        }

        public void EntityUpdate(GameTime GT)
        {
            if (UpdateEntity == true)
            {
                Update(GT);

                for (int i = 0; i < SubEntities.Count; i++)
                {
                    SubEntities[i].Owner = this;
                    SubEntities[i].EntityUpdate(GT);
                }
            }
        }

        public void EntityDraw(SpriteBatch SB)
        {
            if (DrawEntity == true)
            {
                Draw(SB);
            }
        }

        public void SubEntityDraw(SpriteBatch SB)
        {
            for (int i = 0; i < SubEntities.Count; i++)
            {
                SubEntities[i].EntityDraw(SB);
            }
        }

        public virtual void Update(GameTime GT)
        {
        }

        public virtual void Draw(SpriteBatch SB)
        {
            UpdateDrawBox();
            SB.Draw(Texture, DrawBox, Color.White);

        }

        public virtual void UpdateDrawBox()
        {
            DrawBox = new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
        }

        public static Entity GetShallowCopy(Entity E)
        {
            Entity Clone = (Entity)E.MemberwiseClone();
            Clone.SubEntities = new List<Entity>();

            return Clone;
        }

        public void MoveUp()
        {
            GameWorld.GetEntityAt(X, Y).SubEntities.Remove(this);
            GameWorld.PlaceObject(X, Y - 1, this);
        }

        public void MoveDown()
        {
            GameWorld.GetEntityAt(X, Y).SubEntities.Remove(this);
            GameWorld.PlaceObject(X, Y + 1, this);
        }

        public void MoveRight()
        {
            GameWorld.GetEntityAt(X, Y).SubEntities.Remove(this);
            GameWorld.PlaceObject(X + 1, Y, this);
        }

        public void MoveLeft()
        {
            GameWorld.GetEntityAt(X, Y).SubEntities.Remove(this);
            GameWorld.PlaceObject(X - 1, Y, this);
        }
    }
}
