﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Druid
{
    class RoomGenerator
    {
        public static List<List<Tile>> GenerateEmptyRoom(Rectangle roomScale, Tile floor, Tile wall)
        {
            List<List<Tile>> room = new List<List<Tile>>();

            for (int y = 0; y < roomScale.Height; y++)
            {
                List<Tile> tileRow = new List<Tile>();
                for (int x = 0; x < roomScale.Width; x++)
                {
                    tileRow.Add(Tile.GetShallowCopy(floor));
                }
                room.Add(tileRow);
            }

            //add walls
            for (int y = 0; y < room.Count; y++)
            {
                for (int x = 0; x < room[0].Count; x++)
                {
                    if (x == 0 || y == 0 || x == room[0].Count - 1 || y == room.Count - 1)
                    {
                        room[y][x] = Tile.GetShallowCopy(wall);
                    }
                }
            }
            return room;
        }
    }
}
