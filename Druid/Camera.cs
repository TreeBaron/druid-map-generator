﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Druid
{
    class Camera
    {
        public Matrix transform;
        public Viewport view;
        public Vector2 Center;
        public Vector2 Pos;
        public float scale = 0.15f;
        public float Rotation;

        public bool RotationEnabled = false;

        public bool Track_Object_In_Center = true;

        public Camera(Viewport newView)
        {
            view = newView;
        }
        
        public void Update(GameTime GT, Vector2 Position)
        {
            //Pos = new Vector2((Position.X - view.Width / 2f), (Position.Y - view.Height / 2f));

            Center = Position;

            if (RotationEnabled == false)
            {
                Rotation = (float)0;// (Math.PI);
            }


            transform = Matrix.CreateTranslation(new Vector3(-Center.X, -Center.Y, 0)) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateScale(new Vector3(scale, scale, 1)) *
                Matrix.CreateTranslation(new Vector3(view.Width * 0.5f, view.Height * 0.5f, 0));

        }
    }
}
