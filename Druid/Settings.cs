﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Druid
{
    class Settings
    {
        public static int DrawDistance = 30000;

        public static int ScreenWidth;
        public static int ScreenHeight;

        public static Random R = new Random(DateTime.Now.Second);

        public static GameConsole CL;
    }
}
