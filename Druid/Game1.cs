﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Druid
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Druid1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Camera GameCamera;

        GameWorld GW = new GameWorld();

        public Druid1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            //Set to native resolution
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Settings.ScreenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Settings.ScreenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            //Set game to full screen
            graphics.IsFullScreen = false;

            //hd baby?
            graphics.GraphicsProfile = GraphicsProfile.HiDef;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            GameCamera = new Camera(GraphicsDevice.Viewport);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //buildings
            Texture2D archeryrange = Content.Load<Texture2D>("Buildings/archeryrange");
            Texture2D barracks = Content.Load<Texture2D>("Buildings/barracks");
            Texture2D blacksmith = Content.Load<Texture2D>("Buildings/blacksmith");
            Texture2D butcher = Content.Load<Texture2D>("Buildings/butcher");
            Texture2D castle = Content.Load<Texture2D>("Buildings/castle");
            Texture2D housea = Content.Load<Texture2D>("Buildings/housea");
            Texture2D houseb = Content.Load<Texture2D>("Buildings/houseb");
            Texture2D housec = Content.Load<Texture2D>("Buildings/housec");
            Texture2D stable = Content.Load<Texture2D>("Buildings/stable");
            Texture2D tanner = Content.Load<Texture2D>("Buildings/tanner");
            Texture2D tower = Content.Load<Texture2D>("Buildings/tower");
            Texture2D weaver = Content.Load<Texture2D>("Buildings/weaver");

            //custom
            Textures.AddTexture("archeryrange", archeryrange);
            Textures.AddTexture("barracks", barracks);
            Textures.AddTexture("blacksmith", blacksmith);
            Textures.AddTexture("butcher", butcher);
            Textures.AddTexture("castle", castle);
            Textures.AddTexture("housea", housea);
            Textures.AddTexture("houseb", houseb);
            Textures.AddTexture("housec", housec);
            Textures.AddTexture("stable", stable);
            Textures.AddTexture("tanner", tanner);
            Textures.AddTexture("tower", tower);
            Textures.AddTexture("weaver", weaver);

            // TODO: use this.Content to load your game content here
            //water 6, 1
            Textures.AddTexture("grass", Content.Load<Texture2D>("WorldBlocks/grass"));
            Textures.AddTexture("sand", Content.Load<Texture2D>("WorldBlocks/sand"));
            Textures.AddTexture("water", Content.Load<Texture2D>("WorldBlocks/water"));

            TextBox.DefaultFont = Content.Load<SpriteFont>("Fonts/File");

            //begin world setup
            WorldSetup();
        }

        private void GenerateResourceTypes()
        {
            Resource water = new Resource("water");
            Resource soil = new Resource("soil");
            Resource wood = new Resource("wood");
            Resource rock = new Resource("rock");
            Resource people = new Resource("people");
        }

        private void WorldSetup()
        {
            //create neccesary tiles...
            Tile Water = new Tile("water", Textures.GetTexture("water"), 0, 0);
            Tile Grass = new Tile("grass", Textures.GetTexture("grass"), 0, 0);
            Tile Sand = new Tile("sand", Textures.GetTexture("sand"), 0, 0);

            //create resources...
            GenerateResourceTypes();

            //give tiles appropriate resources...
            Water.ResourceAmount = new ResourceCounter(MasterResourceDictionary.GetResource("water"), 1);
            Grass.ResourceAmount = new ResourceCounter(MasterResourceDictionary.GetResource("soil"), 1);


            GW.FillWorld(Grass, 300, 300); //5 gigs = 5000 * 5000
            GW.AddLake(Water, Sand, 8, 12);
            GW.AddRiver(Water, Sand);
            //syntax
            GW.GenerateBeaches(Sand, Water);

            for (int i = 0; i < 500; i++)
            {
                GW.AddResourceSplotch(MasterResourceDictionary.GetResource("people"), 25);
            }

            //castle
            Tile castle = new Tile("castle", Textures.GetTexture("castle"), 0, 0);
            //add some houses :D
            Tile house = new Tile("house", Textures.GetTexture("housea"), 0, 0);

            GW.AddHouses(Tile.GetShallowCopy(house), MasterResourceDictionary.GetResource("people"), 15);
            GW.AddHouses(Tile.GetShallowCopy(castle), MasterResourceDictionary.GetResource("people"), 50);

            //set camera position to center...
            GameCamera.Pos.X = (float)GW.GetWorldXNAWidth() / 2f;
            GameCamera.Pos.Y = (float)GW.GetWorldXNAHeight() / 2f;

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }


        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            GW.UpdateWorld(gameTime, GameCamera.Pos);
            GameCamera.Update(gameTime, GameCamera.Pos);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, GameCamera.transform);
            GW.DrawWorld(spriteBatch, GameCamera.Pos);
            spriteBatch.End();


            if (Keyboard.GetState().IsKeyDown(Keys.End))
            {
                GameCamera.Pos.Y += 60f / 4f;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Home))
            {
                GameCamera.Pos.Y -= 60f / 4f;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.PageDown))
            {
                GameCamera.Pos.X += 60f;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Delete))
            {
                GameCamera.Pos.X -= 60f;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.OemMinus))
            {
                GameCamera.scale -= 0.01f;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.OemPlus))
            {
                GameCamera.scale += 0.01f;
            }

            base.Draw(gameTime);
        }

        //https://gamedev.stackexchange.com/questions/35358/create-a-texture2d-from-larger-image
        protected Texture2D GetTextureTileFromSet32(Texture2D originalTexture, int tileX, int tileY)
        {
            Rectangle sourceRectangle = new Rectangle(tileX * 32, tileY * 32, 32, 32);

            Texture2D cropTexture = new Texture2D(GraphicsDevice, sourceRectangle.Width, sourceRectangle.Height);
            Color[] data = new Color[sourceRectangle.Width * sourceRectangle.Height];
            originalTexture.GetData(0, sourceRectangle, data, 0, data.Length);
            cropTexture.SetData(data);
            return cropTexture;
        }
    }
}
