﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Druid
{
    class TextBox : Entity
    {
        public static SpriteFont DefaultFont;

        public string Text { get; set; }
        public Color FontColor { get; set; }

        public TextBox(string text,Vector2 Pos) : base("Textbox",null,0,0)
        {
            Text = text;
            this.Position = Pos;
            this.MoveWithOwner = true;
        }
        
        public override void Draw(SpriteBatch SB)
        {
            SB.DrawString(DefaultFont, Text, Position, Color.White);
        }

        public override void Update(GameTime GT)
        {
            if (MoveWithOwner == true && Owner != null)
            {
                this.Position = Owner.Position;
            }
        }

    }
}
