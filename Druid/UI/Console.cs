﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Druid
{
    class GameConsole : TextBox
    {
        Camera GameCamera;

        private KeyboardState LastKS = Keyboard.GetState();

        private KeyboardState LastKS2 = Keyboard.GetState();

        private string buffer = "Console:";

        private int BackspaceBuildup = 0;

        private int Buildup = 0;

        List<TextBox> TextBoxes = new List<TextBox>();
        List<Vector2> TextBoxesRelativePositions = new List<Vector2>();

        public GameConsole(string text,Vector2 pos, Camera gamecam):base(text,pos)
        {
            GameCamera = gamecam;
            text = "";
        }

        public override void Update(GameTime GT)
        {
            Text = buffer;
            //do nothing...
            base.Update(GT);
            //double check position is corrected
            this.Position = GameCamera.Pos; //this is room center...
            double yboi = (Settings.ScreenHeight / 2) * 0.9;
            double xboi = (Settings.ScreenWidth / 2) * -1 * 0.9;
            this.Position += new Vector2((int)xboi, (int)yboi);

            //get keyboard input and put it into buffer...
            KeyboardState KS = Keyboard.GetState();

            Microsoft.Xna.Framework.Input.Keys [] keyz = KS.GetPressedKeys();

            //lord forgive me for being dumb
            List<Keys> Letters = new List<Keys>()
            {
                Keys.A,
                Keys.B,
                Keys.C,
                Keys.D,
                Keys.E,
                Keys.F,
                Keys.G,
                Keys.H,
                Keys.I,
                Keys.J,
                Keys.K,
                Keys.L,
                Keys.M,
                Keys.N,
                Keys.O,
                Keys.P,
                Keys.Q,
                Keys.R,
                Keys.S,
                Keys.T,
                Keys.U,
                Keys.V,
                Keys.W,
                Keys.X,
                Keys.Y,
                Keys.Z,
                Keys.Space
            };
            
            foreach(Keys KP in keyz)
            {
                if (KS.IsKeyDown(KP) && LastKS.IsKeyDown(KP) == false && Buildup < 100)
                {
                    if (Letters.Contains(KP))
                    {
                        string let = KP.ToString().ToLower();
                        if (let == "space")
                        {
                            buffer += " ";
                        }
                        else
                        {
                            buffer += let;
                        }
                    }

                }

                /*
                //long keypress spams key
                if (KS.IsKeyDown(KP) && LastKS.IsKeyDown(KP) && LastKS2.IsKeyDown(KP))
                {
                    Buildup++;

                    if (Letters.Contains(KP) && Buildup > 100)
                    {
                        string let = KP.ToString().ToLower();
                        if (let == "space")
                        {
                            buffer += " ";
                        }
                        else
                        {
                            buffer += let;
                        }
                        Buildup = (Buildup /2) -1;
                    }
                }
                */
            }

            if(KS.IsKeyDown(Keys.Enter) && LastKS.IsKeyDown(Keys.Enter) == false)
            {
                string origin = buffer;
                buffer = "";
                ScrollConsole(origin);
            }

            BackSpaceHandling(KS);

            LastKS2 = LastKS;
            LastKS = KS;

            UpdateTextBoxes(GT);
            VerifyTextBoxPositions();

        }

        private void ScrollConsole(string origin)
        {
            //double check position is corrected
            this.Position = GameCamera.Pos; //this is room center...
            double yboi = (Settings.ScreenHeight /2)*0.9;
            double xboi = (Settings.ScreenWidth / 2) * -1 * 0.9;
            this.Position += new Vector2((int)xboi, (int)yboi);

            //add text scroll upwards...
            Vector2 StringHeight = TextBox.DefaultFont.MeasureString("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
            int yadjust = (int)(StringHeight.Y * -1.5);
            Vector2 Adjust = new Vector2(0, yadjust);
            TextBox TB = new TextBox(origin, this.Position + Adjust);
            TextBoxes.Add(TB);

            Vector2 RelativePosition = new Vector2(this.Position.X- GameCamera.Pos.X,this.Position.Y -GameCamera.Pos.Y);
            TextBoxesRelativePositions.Add(RelativePosition);

            //move up all text boxes
            for(int i = 0; i < TextBoxesRelativePositions.Count; i++)
            {
                TextBoxesRelativePositions[i] += new Vector2(0,yadjust);
            }

            //remove old text boxes
            if (TextBoxes.Count > 50)
            {
                TextBoxes.RemoveAt(0);
                TextBoxesRelativePositions.RemoveAt(0);
            }
        }

        private void VerifyTextBoxPositions()
        {
            for(int i = 0; i < TextBoxes.Count; i++)
            {
                TextBoxes[i].Position = GameCamera.Pos + TextBoxesRelativePositions[i];
            }
        }

        public void WriteLine(string origin)
        {
            ScrollConsole(origin);
        }

        public void Clear()
        {
            TextBoxes.Clear();
            TextBoxesRelativePositions.Clear();
        }

        private void UpdateTextBoxes(GameTime GT)
        {
            foreach(TextBox TB in TextBoxes)
            {
                TB.Update(GT);
            }
        }

        private void DrawTextBoxes(SpriteBatch SB)
        {
            foreach (TextBox TB in TextBoxes)
            {
                TB.Draw(SB);
            }
        }

        public override void Draw(SpriteBatch SB)
        {
            base.Draw(SB);
            DrawTextBoxes(SB);
        }

        private void BackSpaceHandling(KeyboardState KS)
        {
            if (KS.IsKeyDown(Keys.Back) && LastKS.IsKeyDown(Keys.Back) == false && buffer.Length > 0)
            {
                buffer = buffer.Substring(0, buffer.Length - 1);
            }

            if (KS.IsKeyDown(Keys.Back))
            {
                BackspaceBuildup++;
            }
            else
            {
                BackspaceBuildup = 0;
            }

            if (BackspaceBuildup >= 20 && buffer.Length >= 1)
            {
                buffer = buffer.Substring(0, buffer.Length - 1);
            }
        }
    }
}
